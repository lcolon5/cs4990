from django.contrib import admin
from polls.models import Poll, Choice

class PollAdmin(admin.ModelAdmin):
	#Change object page
	fields = ['question', 'author', 'pub_date'] 
	#list of objects page
	list_display = ('question', 'author', 'pub_date',)

class ChoiceAdmin(admin.ModelAdmin):
	list_display = ('answer', 'poll', 'count',)

admin.site.register(Poll, PollAdmin)
admin.site.register(Choice, ChoiceAdmin)
